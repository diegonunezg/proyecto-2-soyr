#include <ios>
#include <iterator>
#include <ostream>
#include <semaphore.h>
#include <stdio.h>
#include <string>
#include <cstdlib>
#include <unistd.h>
#include <thread>
#include <vector>
#include <fstream>
#include <iostream>
#include <mutex>

typedef void(*FunctionPointer)();

using namespace std;

ofstream gestor_archivo;

int contador = 60;
mutex contador_mtx;

int glucosa= 250, glucosa_6_p = 10, fructosa_6_p = 10;
int fructosa_1_6_bp = 10, dihidroxiacetonafosfato = 10;
int gliceraldehido_3_p = 10, _1_3_bifosfoglicerato = 10;
int _3_fosfoglicerato = 10, _2_fosfoglicerato = 10;
int fosfoenolpiruvato = 10, piruvato = 10;

sem_t glucosa_sem, glucosa_6_p_sem, fructosa_6_p_sem;
sem_t fructosa_1_6_bp_sem, dihidroxiacetonafosfato_sem;
sem_t gliceraldehido_3_p_sem, _1_3_bifosfoglicerato_sem;
sem_t _3_fosfoglicerato_sem, _2_fosfoglicerato_sem;
sem_t fosfoenolpiruvato_sem, piruvato_sem;

mutex glucosa_mtx, glucosa_6_p_mtx, fructosa_6_p_mtx;
mutex fructosa_1_6_bp_mtx, dihidroxiacetonafosfato_mtx;
mutex gliceraldehido_3_p_mtx, _1_3_bifosfoglicerato_mtx;
mutex _3_fosfoglicerato_mtx, _2_fosfoglicerato_mtx;
mutex fosfoenolpiruvato_mtx, piruvato_mtx;


void print_metabolitos(){

    string text = "";
    text += to_string(61-contador) + ",glucosa," + to_string(glucosa) + "\n";
    text += to_string(61-contador) + ",glucosa_6_p," + to_string(glucosa_6_p) + "\n";
    text += to_string(61-contador) + ",fructosa_6_p," + to_string(fructosa_6_p) + "\n";
    text += to_string(61-contador) + ",fructosa_1_6_bp," + to_string(fructosa_1_6_bp) + "\n";
    text += to_string(61-contador) + ",dihidroxiacetonafosfato," + to_string(dihidroxiacetonafosfato) + "\n";
    text += to_string(61-contador) + ",gliceraldehido_3_p," + to_string(gliceraldehido_3_p) + "\n";
    text += to_string(61-contador) + ",1_3_bifosfoglicerato," + to_string(_1_3_bifosfoglicerato) + "\n";
    text += to_string(61-contador) + ",3_fosfoglicerato," + to_string(_3_fosfoglicerato) + "\n";
    text += to_string(61-contador) + ",2_fosfoglicerato," + to_string(_2_fosfoglicerato) + "\n";
    text += to_string(61-contador) + ",fosfoenolpiruvato," + to_string(fosfoenolpiruvato) + "\n";
    text += to_string(61-contador) + ",piruvato," + to_string(piruvato);

  gestor_archivo.open("glucolisis.csv", ios_base::app);
    gestor_archivo << text << endl;
    gestor_archivo.close();

}

void timer(){
    while (contador > 0) {
        contador--;
        sleep(1);
        print_metabolitos();
    }
}


void ciclar(void (*func)()){
    while (true) {
        if (contador <= 0){
            break;
        }
        func();
        sleep(rand()%2 +1);
    }
}


void Hexocinasa(){
    sem_wait(&glucosa_sem);
    glucosa_mtx.lock();
    glucosa--;
    glucosa_mtx.unlock();

    glucosa_6_p_mtx.lock();
    glucosa_6_p++;
    glucosa_6_p_mtx.unlock();
    sem_post(&glucosa_6_p_sem);
}

void Fosfoglucosa_isomerasa(){
    sem_wait(&glucosa_6_p_sem);
    glucosa_6_p_mtx.lock();
    glucosa_6_p--;
    glucosa_6_p_mtx.unlock();
    
    fructosa_6_p_mtx.lock();
    fructosa_6_p++;
    fructosa_6_p_mtx.unlock();
    sem_post(&fructosa_6_p_sem);
}

void Fosfofrutosa_cinasa(){
    sem_wait(&fructosa_6_p_sem);
    fructosa_6_p_mtx.lock();
    fructosa_6_p--;
    fructosa_6_p_mtx.unlock();

    fructosa_1_6_bp_mtx.lock();
    fructosa_1_6_bp++;
    fructosa_1_6_bp_mtx.unlock();
    sem_post(&fructosa_1_6_bp_sem);
}

void Fructosa_bifosfato_aldolasa(){
    sem_wait(&fructosa_1_6_bp_sem);
    fructosa_1_6_bp_mtx.lock();
    fructosa_1_6_bp--;
    fructosa_1_6_bp_mtx.unlock();

    dihidroxiacetonafosfato_mtx.lock();
    dihidroxiacetonafosfato++;
    dihidroxiacetonafosfato_mtx.unlock();
    sem_post(&dihidroxiacetonafosfato_sem);

    gliceraldehido_3_p_mtx.lock();
    gliceraldehido_3_p++;
    gliceraldehido_3_p_mtx.unlock();
    sem_post(&gliceraldehido_3_p_sem);
}

void Triosa_fosfato_isomerasa(){
    sem_wait(&dihidroxiacetonafosfato_sem);
    dihidroxiacetonafosfato_mtx.lock();
    dihidroxiacetonafosfato--;
    dihidroxiacetonafosfato_mtx.unlock();

    gliceraldehido_3_p_mtx.lock();
    gliceraldehido_3_p++;
    gliceraldehido_3_p_mtx.unlock();
    sem_post(&gliceraldehido_3_p_sem);
}

void Gliceraldehido_3_fosfato_deshidrogenasa(){
    sem_wait(&gliceraldehido_3_p_sem);
    gliceraldehido_3_p_mtx.lock();
    gliceraldehido_3_p--;
    gliceraldehido_3_p_mtx.unlock();

    _1_3_bifosfoglicerato_mtx.lock();
    _1_3_bifosfoglicerato++;
    _1_3_bifosfoglicerato_mtx.unlock();
    sem_post(&_1_3_bifosfoglicerato_sem);
}

void Fosfoglicerato_cinasa(){
    sem_wait(&_1_3_bifosfoglicerato_sem);
    _1_3_bifosfoglicerato_mtx.lock();
    _1_3_bifosfoglicerato--;
    _1_3_bifosfoglicerato_mtx.unlock();

    _3_fosfoglicerato_mtx.lock();
    _3_fosfoglicerato++;
    _3_fosfoglicerato_mtx.unlock();
    sem_post(&_3_fosfoglicerato_sem);
}

void Fosfoglicerato_mutasa(){
    sem_wait(&_3_fosfoglicerato_sem);
    _3_fosfoglicerato_mtx.lock();
    _3_fosfoglicerato--;
    _3_fosfoglicerato_mtx.unlock();

    _2_fosfoglicerato_mtx.lock();
    _2_fosfoglicerato++;
    _2_fosfoglicerato_mtx.unlock();
    sem_post(&_2_fosfoglicerato_sem);
}

void Enolasa(){
    sem_wait(&_2_fosfoglicerato_sem);
    _2_fosfoglicerato_mtx.lock();
    _2_fosfoglicerato--;
    _2_fosfoglicerato_mtx.unlock();

    fosfoenolpiruvato_mtx.lock();
    fosfoenolpiruvato++;
    fosfoenolpiruvato_mtx.unlock();
    sem_post(&fosfoenolpiruvato_sem);
}

void Piruvato_cinasa(){
    sem_wait(&fosfoenolpiruvato_sem);
    fosfoenolpiruvato_mtx.lock();
    fosfoenolpiruvato--;
    fosfoenolpiruvato_mtx.unlock();

    piruvato_mtx.lock();
    piruvato++;
    piruvato_mtx.unlock();
    sem_post(&piruvato_sem);
}


int main(){

    srand(time(0));

    gestor_archivo.open("glucolisis.csv");
    gestor_archivo << "Tiempo,metabolito,Cantidad" << endl;
    gestor_archivo.close();

    sem_init(&glucosa_sem, 0, glucosa);
    sem_init(&glucosa_6_p_sem, 0, glucosa_6_p);
    sem_init(&fructosa_6_p_sem, 0, fructosa_6_p);
    sem_init(&fructosa_1_6_bp_sem, 0, fructosa_1_6_bp);
    sem_init(&dihidroxiacetonafosfato_sem, 0, dihidroxiacetonafosfato);
    sem_init(&gliceraldehido_3_p_sem, 0, gliceraldehido_3_p);
    sem_init(&_1_3_bifosfoglicerato_sem, 0, _1_3_bifosfoglicerato);
    sem_init(&_3_fosfoglicerato_sem, 0, _3_fosfoglicerato);
    sem_init(&_2_fosfoglicerato_sem, 0, _2_fosfoglicerato);
    sem_init(&fosfoenolpiruvato_sem, 0, fosfoenolpiruvato);
    sem_init(&piruvato_sem, 0, piruvato);

    print_metabolitos();

    FunctionPointer functionPointers[] = {&Hexocinasa, &Fosfoglucosa_isomerasa, &Fosfofrutosa_cinasa, &Fructosa_bifosfato_aldolasa, &Triosa_fosfato_isomerasa, &Gliceraldehido_3_fosfato_deshidrogenasa, &Fosfoglicerato_cinasa, &Fosfoglicerato_mutasa, &Enolasa, &Piruvato_cinasa};

    thread funciones[45];

    for (int i = 0; i < 5; i++){
        for (int j = 0; j < 3; j++){
            funciones[3*i +j] = thread(ciclar, (functionPointers[i]));
        }
    }

    for (int i = 0; i < 5; i++){
        for (int j = 0; j < 6; j++){
            funciones[6*i +j+15] = thread(ciclar, (functionPointers[i+5]));
        }
    }
    
    thread tim = thread(timer);

    for (int i = 0; i < 45; i++){
        funciones[i].join();
    }

    tim.join();

    sem_destroy(&glucosa_sem);
    sem_destroy(&glucosa_6_p_sem);
    sem_destroy(&fructosa_6_p_sem);
    sem_destroy(&fructosa_1_6_bp_sem);
    sem_destroy(&dihidroxiacetonafosfato_sem);
    sem_destroy(&gliceraldehido_3_p_sem);
    sem_destroy(&_1_3_bifosfoglicerato_sem);
    sem_destroy(&_3_fosfoglicerato_sem);
    sem_destroy(&_2_fosfoglicerato_sem);
    sem_destroy(&fosfoenolpiruvato_sem);
    sem_destroy(&piruvato_sem);

    return 0;
}
